# PTB WEEKLY ASSESMENT WEEK 1



## Getting started

1.  Terdapat elemen header, footer, main, article, dan aside di berkas HTML.
2.  Masing-masing elemen wajib berisi konten yang peruntukkannya sesuai dengan elemen tersebut (menerapkan konsep semantic HTML dalam menyusun struktur website).
3.  Sebagai contoh: Header berisi judul dan navigation. Sedangkan konten artikel tidak boleh berada pada Header.
4.  Wajib menampilkan identitas diri (biodata diri) yang minimal harus berisi foto asli diri dan nama sesuai profil ANDA. Identitas diri wajib ditampilkan dalam elemen aside
5.  Menyusun layout dengan menggunakan float atau flexbox.
6.  Tema yang ditampilkan bebaS.


![Screenshot](screenshot.png)
